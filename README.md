# B2 File Storage

## Getting started

All you need to do is call the following call to upload a file.

```
$upload=$b2->uploadFile($filepath,"folder");
$filename=$upload["fileName"];
$b2id=$upload["fileId"];
```

Store the details in a database to manage the file later.

To delete a file the following code will work.

```
$delete=$b2->deleteFile("filename","b2id");
```
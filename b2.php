<?php

$b2 = new B2Uploader([
  'accountId'         => '',
  'applicationKey'    => '',
  'bucketId'          => ''
]);

$docroot=$_SERVER["DOCUMENT_ROOT"];
$b2key=$docroot."/.b2key";
$b2urlapi=$docroot."/.b2urlapi";
$b2urldown=$docroot."/.b2urldown";
$b2authtoken=$docroot."/.b2authtoken";
$b2uploadurl=$docroot."/.b2uploadurl";

//--See if the file for B2 should be cleared
if (is_file($b2key)){
	if (time()-filemtime($b2key) > 10 * 3600){ //In Hours
		unlink($b2key);
		unlink($b2key);
		unlink($b2urlapi);
		unlink($b2urldown);
		unlink($b2authtoken);
		unlink($b2uploadurl);
	}
}

//--You can call this to purge all key files any time
function purgeb2files(){
	global $b2key;
	global $b2urlapi;
	global $b2urldown;
	global $b2authtoken;
	global $b2uploadurl;
	
	unlink($b2key);
	unlink($b2urlapi);
	unlink($b2urldown);
	unlink($b2authtoken);
	unlink($b2uploadurl);
}

class B2Uploader{

	//--Variables set by user
	private $bucketId;

	//--Authorization stuff
	private $accountId;
	private $applicationKey;
	private $credentials;
	private $authorizationToken;	//--Filled in by Authorize() function

	//--URLs
	const AUTHORIZE_URL = 'https://api.backblazeb2.com/b2api/v2/b2_authorize_account';
	private $apiUrl;
	private $downloadUrl;
	private $uploadUrl;

	public function __construct(array $config){
		//--Check that we have the required parameters
		$requireParameters = ['accountId', 'applicationKey', 'bucketId'];

		foreach($requireParameters as $parameter){
			if(!isset($config[$parameter])){
				throw new InvalidArgumentException('Parameter ' . $parameter . ' was not passed.');
			}
		}

		//--Set the configuration
		$this->setAccountId($config['accountId']);
		$this->setApplicationKey($config['applicationKey']);
		$this->setBucketId($config['bucketId']);
	}

	public function getFilesInBucket(){
		$this->authorize();

		$url = $this->getApiUrl() .  "/b2api/v2/b2_list_file_names";

		$headers = array();
		$headers[] = "Authorization: " . $this->loadb2key();

		$postFields = array("bucketId" => $this->getBucketId());

		return $this->curlRequest('POST', $url, $headers, $postFields);
	}

	private function getUploadURL(){
		if ($this->getb2uploadUrl()==false){
			$url = $this->getApiUrl() .  "/b2api/v2/b2_get_upload_url";
	
			//--Set the headers
			$headers = array();
			$headers[] = "Authorization: " . $this->getAuthorizationToken();
	
			//--Post the bucketId fields
			$postFields = array("bucketId" => $this->getBucketId());
	
			//--Ask CURL to make the request
			$response = $this->curlRequest('POST', $url, $headers, $postFields);
	
			$this->setb2uploadUrl($response->uploadUrl);
			$this->setAuthorizationToken($response->authorizationToken);
		}
	}

	private function authorize(){
		if ($this->loadb2key()==false){
			//--Set the headers
			$headers = array();
			$headers[] = "Accept: application/json";
			$headers[] = "Authorization: Basic " . $this->getCredentials();

			//--Make the request
			$response = $this->curlRequest('GET', self::AUTHORIZE_URL, $headers);

			//--Save the JSON data in our object
			$this->setApiUrl($response->apiUrl);
			$this->setAuthorizationToken($response->authorizationToken);
			$this->setDownloadUrl($response->downloadUrl);

			$this->setb2key($response->authorizationToken);
			$this->setb2urlapi($response->apiUrl);
			$this->setb2urldown($response->downloadUrl);
		}
	}

	public function getFileInfo($fileId) {
		//--Authorize
		$this->authorize();

		$url = $this->getApiUrl() .  "/b2api/v2/b2_get_file_info";

		//--Add headers
		$headers = array();
		$headers[] = "Authorization: " . $this->loadb2key();

		$postFields = array("fileId" => $fileId);
		//--Make the request
		$response = $this->curlRequest('POST', $url, $headers, $postFields);
		$response->versions = $this->getFileVersions($response->fileName,$fileId);
		return $response;
	}

	public function deleteFile($fileName,$fileId,$purge_all_versions=false) {
		//purge all versions is a commodity method
		if (!$purge_all_versions) {
			//--Authorize
			$this->authorize();
			$url = $this->getApiUrl() .  "/b2api/v2/b2_delete_file_version";
			
			//--Add headers
			$headers = array();
			$headers[] = "Authorization: " . $this->loadb2key();
			$postFields = array("fileName" => $fileName,"fileId" => $fileId);
			
			//--Make the request
			$response = $this->curlRequest('POST', $url, $headers,$postFields);
			return $response;
		}else{
			$versions = $this->getFileVersions($p->files[0]->fileName,$p->files[0]->fileId);
			foreach($versions as $version) {
				if (!empty($version[0]->fileName)) {
					$this->deleteFile($version[0]->fileName,$version[0]->fileId);
				}
			}
		}

	}

	public function getFileVersions($startFileName='',$startFileId='',$maxFileCount=0,$prefix="",$delimiter="") {
		//--Authorize
		$this->authorize();
		$url = $this->getApiUrl() .  "/b2api/v2/b2_list_file_versions";

		$postFields = array("bucketId" => $this->getBucketId());
		if (!empty($startFileName)) $postFields["startFileName"] = $startFileName;
		if (!empty($startFileId)) $postFields["startFileId"] = $startFileId;
		if (!empty($maxFileCount)) $postFields["maxFileCount"] = $maxFileCount;
		if (!empty($prefix)) $postFields["prefix:"] = $prefix;
		if (!empty($delimiter)) $postFields["delimiter:"] = $delimiter;

		//--Add headers
		$headers = array();
		$headers[] = "Authorization: " . $this->loadb2key();

		//--Make the request
		$response = $this->curlRequest('POST', $url, $headers,$postFields);
		return $response;
	}

	public function uploadFile($path, $folder = ''){
		//--Authorize
		$this->authorize();

		//--Get the uploadUrl
		$this->getUploadURL();

		//--Open the file for reading
		$handle = fopen($path, 'r');
		$read_file = fread($handle,filesize($path));

		//--Generate SHA1 hash
		$sha1_of_file_data = sha1_file($path);

		//--Check that folder ends with trailing slash
		if($folder != '' && substr($folder, -1) != '/'){
			$folder .= '/';
		}

		//--Add headers
		$headers = array();
		$headers[] = "Authorization: " . $this->getAuthorizationToken();
		$headers[] = "X-Bz-File-Name: " . $folder . basename($path);
		$headers[] = "Content-Type: " . 'b2/x-auto';
		$headers[] = "X-Bz-Content-Sha1: " . $sha1_of_file_data;

		//--Make the request
		$response = $this->curlRequest('POST', $this->getb2uploadUrl(), $headers, $read_file, true);

		return $response;
	}

	private function curlRequest($httpMethod = 'GET', $url, array $headers = [], $postFields = [], $uploadFile = false){
		$session = curl_init($url);

		//--Add the file contents if we're uploading
		if($uploadFile){
			curl_setopt($session, CURLOPT_POSTFIELDS, $postFields);
		}else{
			//--Add post fields
			if(count($postFields) != 0){
				$postFields = json_encode($postFields);
				curl_setopt($session, CURLOPT_POSTFIELDS, $postFields);
			}
		}

		//--Add headers
		curl_setopt($session, CURLOPT_HTTPHEADER, $headers);

		//--Set HTTP method
		if($httpMethod === 'GET'){
			curl_setopt($session, CURLOPT_HTTPGET, true);
		}else{
			curl_setopt($session, CURLOPT_POST, true);
		}

		//--Receive server responses
		curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

		//--Fire the request
		$server_output = curl_exec($session);

		//--Check if we got a HTTP 200 status code
		if(curl_getinfo($session, CURLINFO_HTTP_CODE) != 200){
			//Re auth so keys are refreshed on file
			$this->authorize();
			$this->getUploadURL();
			purgeb2files();
			
			throw new RuntimeException("Didn't get a 200 response code: ".json_encode($headers)." ".$this->getAuthorizationToken()." " . $server_output);
		}

		//--Bye bye CURL!
		curl_close ($session);

		//--Decode & return the JSON
		return json_decode($server_output);
	}

	private function getCredentials(){
		return base64_encode($this->accountId . ':' . $this->applicationKey);
	}

	public function setApplicationKey($key){
		$this->applicationKey = $key;
	}

	public function setAccountId($accountId){
		$this->accountId = $accountId;
	}

	public function setBucketId($id){
		$this->bucketId = $id;
	}

	private function getBucketId(){
		return $this->bucketId;
	}

	private function setApiUrl($url){
		$this->apiUrl = $url;
	}

	private function getApiUrl(){
		if ($this->loadb2urlapi()!=false){
			$this->apiUrl=$this->loadb2urlapi();
		}
		return $this->apiUrl;
	}
	
	private function setb2uploadUrl($token){
		$scorefile = fopen(".b2uploadurl", "w");
		fwrite($scorefile, $token);
		fclose($scorefile);
	}
	
	private function getb2uploadUrl(){
		if (file_exists(".b2uploadurl")){
			$file=fopen(".b2uploadurl", "r");
			$response=fread($file,filesize(".b2uploadurl"));
			return $response;
		}else{
			return false;
		}
	}

	private function setauthorizationToken($token){
		$scorefile = fopen(".b2authtoken", "w");
		fwrite($scorefile, $token);
		fclose($scorefile);
	}

	private function getAuthorizationToken(){
		if (file_exists(".b2authtoken")){
			$file=fopen(".b2authtoken", "r");
			$response=fread($file,filesize(".b2authtoken"));
			return $response;
		}else{
			return false;
		}
	}

	private function setDownloadUrl($url){
		$this->downloadUrl = $url;
	}


	private function setb2key($value){
		$scorefile = fopen(".b2key", "w");
		fwrite($scorefile, $value);
		fclose($scorefile);
	}

	private function loadb2key(){
		if (file_exists(".b2key")){
			$file=fopen(".b2key", "r");
			$response=fread($file,filesize(".b2key"));
			return $response;
		}else{
			return false;
		}
	}

	private function setb2urlapi($value){
		$scorefile = fopen(".b2urlapi", "w");
		fwrite($scorefile, $value);
		fclose($scorefile);
	}

	private function loadb2urlapi(){
		if (file_exists(".b2urlapi")){
			$file=fopen(".b2urlapi", "r");
			$response=fread($file,filesize(".b2urlapi"));
			return $response;
		}else{
			return false;
		}
	}

	private function setb2urldown($value){
		$scorefile = fopen(".b2urldown", "w");
		fwrite($scorefile, $value);
		fclose($scorefile);
	}

	private function loadb2urldown(){
		if (file_exists(".b2urldown")){
			$file=fopen(".b2urldown", "r");
			$response=fread($file,filesize(".b2urldown"));
			return $response;
		}else{
			return false;
		}
	}

}
